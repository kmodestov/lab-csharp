﻿namespace labCalculator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.laA = new System.Windows.Forms.Label();
            this.B = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.laSumma = new System.Windows.Forms.Label();
            this.laUmnozh = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // laA
            // 
            this.laA.AutoSize = true;
            this.laA.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.laA.Location = new System.Drawing.Point(22, 46);
            this.laA.Name = "laA";
            this.laA.Size = new System.Drawing.Size(17, 17);
            this.laA.TabIndex = 0;
            this.laA.Text = "A";
            // 
            // B
            // 
            this.B.AutoSize = true;
            this.B.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.B.ForeColor = System.Drawing.SystemColors.Desktop;
            this.B.Location = new System.Drawing.Point(22, 95);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(17, 17);
            this.B.TabIndex = 1;
            this.B.Text = "B";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(104, 46);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(104, 95);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(25, 165);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Сложение";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // laSumma
            // 
            this.laSumma.AutoSize = true;
            this.laSumma.Location = new System.Drawing.Point(22, 211);
            this.laSumma.Name = "laSumma";
            this.laSumma.Size = new System.Drawing.Size(42, 13);
            this.laSumma.TabIndex = 5;
            this.laSumma.Text = "Summa";
            // 
            // laUmnozh
            // 
            this.laUmnozh.AutoSize = true;
            this.laUmnozh.Location = new System.Drawing.Point(188, 211);
            this.laUmnozh.Name = "laUmnozh";
            this.laUmnozh.Size = new System.Drawing.Size(68, 13);
            this.laUmnozh.TabIndex = 6;
            this.laUmnozh.Text = "Proizvedenie";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(191, 165);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Умножение";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.laUmnozh);
            this.Controls.Add(this.laSumma);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.B);
            this.Controls.Add(this.laA);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laA;
        private System.Windows.Forms.Label B;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label laSumma;
        private System.Windows.Forms.Label laUmnozh;
        private System.Windows.Forms.Button button2;
    }
}

