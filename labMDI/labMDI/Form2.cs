﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labMDI
{
    public partial class fmNote : Form
    {
        public fmNote()
        {
            InitializeComponent();
        }

        private void fmNote_Load(object sender, EventArgs e)
        {
            InitializeComponent();
            Text = DateTime.Now.ToString();
        }
    }
}
