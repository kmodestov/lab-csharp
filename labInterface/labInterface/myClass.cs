﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labInterface
{
    interface IAge
    {
        void ShowAge();
    }
    interface IFIO
    {
        void ShowFIO();
    }
    class myClass : IAge, IFIO
    {
        public void ShowAge()
        {
            Console.WriteLine("ShowAge");
        }
        public void ShowFIO()
        {
            Console.WriteLine("ShowFio");
        }
    }
    class Write : IAge, IFIO
    {
        public void ShowAge()
        {
            Console.WriteLine("22");
        }
        public void ShowFIO()
        {
            Console.WriteLine("Sasha");  
        }
    }
}
