﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labFactorial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Factorial(3));
        }

        private static int Factorial(int v)
        {
            Console.WriteLine("v={0}", v);
            if (v==0)
            {
                return 1;
            }
            else
            {
                return v * Factorial(v - 1);
            }
        }
    }
}
