﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1;
            while (i<4)
            {
                Console.WriteLine("i={0}", i);
                i++;
            }
            Console.WriteLine("end");
        }
    }
}
