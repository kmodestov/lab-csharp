﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labFor
{
    class Program
    {
        static void Main(string[] args)
        {
            AAA(6);
            AAA(3);
        }

        private static void AAA(int v)
        {
            for (int i = 1; i <= v; i += 2)
            {
                Console.WriteLine("i={0}", i);
            }
            Console.WriteLine();
        }
    }
}
