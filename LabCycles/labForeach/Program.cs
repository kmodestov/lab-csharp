﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labForeach
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] x = { "abcdef", "qwerty", "zxcvb" };
            int r = 0;
            foreach (string i in x)
            {
                r = r + 1;
                Console.WriteLine(i);
            }
            Console.WriteLine();
            Console.WriteLine("sum={0}", r);
        }
    }
}
