﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labRemeberNumber
{
    class Games
    {
        private Timer tm;
        public int CountCorrect { get; protected set; }
        public int CountWrong { get; protected set; }
        public int Code { get; protected set; }

        public event EventHandler GoScreenRemember;
        public event EventHandler GoScreenAnswer;
        public Games()
        {
            tm = new Timer();
            tm.Enabled = false;
            tm.Interval = 2000;
            tm.Tick += Event_Tick;
        }
        private void Event_Tick(object sender, EventArgs e)
        {
            tm.Enabled = false;
            if (GoScreenAnswer != null)
                GoScreenAnswer(this, EventArgs.Empty);
        }
        public void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            DoContinue();
        }
        public void DoContinue()
        {
            Random rnd = new Random();
            Code = rnd.Next(100000, 999999);
            tm.Enabled = true;
            if (GoScreenRemember != null)
                GoScreenRemember(this, EventArgs.Empty);

        }
        public void DoAnswer(int v)
        {
            if (v == Code)
                CountCorrect++;
            else
                CountWrong++;
            DoContinue();
        }
    }
}
