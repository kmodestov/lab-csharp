﻿namespace labRemeberNumber
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.laYes = new System.Windows.Forms.Label();
            this.laNo = new System.Windows.Forms.Label();
            this.laZapomni = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.laChislo = new System.Windows.Forms.Label();
            this.laVspomni = new System.Windows.Forms.Label();
            this.tbVvod = new System.Windows.Forms.TextBox();
            this.buGo = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.laNo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.laYes, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(463, 63);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // laYes
            // 
            this.laYes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laYes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.laYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laYes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.laYes.Location = new System.Drawing.Point(3, 0);
            this.laYes.Name = "laYes";
            this.laYes.Size = new System.Drawing.Size(225, 63);
            this.laYes.TabIndex = 1;
            this.laYes.Text = "Верно = 0";
            this.laYes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laNo
            // 
            this.laNo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.laNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laNo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.laNo.Location = new System.Drawing.Point(234, 0);
            this.laNo.Name = "laNo";
            this.laNo.Size = new System.Drawing.Size(226, 63);
            this.laNo.TabIndex = 2;
            this.laNo.Text = "Неверно = 0";
            this.laNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laZapomni
            // 
            this.laZapomni.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laZapomni.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laZapomni.Location = new System.Drawing.Point(149, 150);
            this.laZapomni.Name = "laZapomni";
            this.laZapomni.Size = new System.Drawing.Size(189, 48);
            this.laZapomni.TabIndex = 1;
            this.laZapomni.Text = "Запомни число";
            this.laZapomni.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.laChislo);
            this.panel1.Location = new System.Drawing.Point(122, 201);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(242, 95);
            this.panel1.TabIndex = 2;
            // 
            // laChislo
            // 
            this.laChislo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laChislo.Font = new System.Drawing.Font("Microsoft Sans Serif", 45F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laChislo.Location = new System.Drawing.Point(0, 0);
            this.laChislo.Name = "laChislo";
            this.laChislo.Size = new System.Drawing.Size(238, 91);
            this.laChislo.TabIndex = 0;
            this.laChislo.Text = "000000";
            this.laChislo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laVspomni
            // 
            this.laVspomni.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laVspomni.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laVspomni.Location = new System.Drawing.Point(149, 333);
            this.laVspomni.Name = "laVspomni";
            this.laVspomni.Size = new System.Drawing.Size(189, 48);
            this.laVspomni.TabIndex = 3;
            this.laVspomni.Text = "Вспомни число";
            this.laVspomni.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbVvod
            // 
            this.tbVvod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbVvod.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbVvod.Location = new System.Drawing.Point(85, 384);
            this.tbVvod.Name = "tbVvod";
            this.tbVvod.Size = new System.Drawing.Size(320, 31);
            this.tbVvod.TabIndex = 4;
            // 
            // buGo
            // 
            this.buGo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buGo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buGo.Location = new System.Drawing.Point(166, 421);
            this.buGo.Name = "buGo";
            this.buGo.Size = new System.Drawing.Size(151, 47);
            this.buGo.TabIndex = 5;
            this.buGo.Text = "Проверить";
            this.buGo.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(487, 480);
            this.Controls.Add(this.buGo);
            this.Controls.Add(this.tbVvod);
            this.Controls.Add(this.laVspomni);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.laZapomni);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(503, 519);
            this.Name = "fm";
            this.Text = "labRememberNumber";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label laNo;
        private System.Windows.Forms.Label laYes;
        private System.Windows.Forms.Label laZapomni;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label laChislo;
        private System.Windows.Forms.Label laVspomni;
        private System.Windows.Forms.TextBox tbVvod;
        private System.Windows.Forms.Button buGo;
    }
}

