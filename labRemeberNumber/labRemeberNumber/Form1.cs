﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labRemeberNumber
{
    public partial class fm : Form
    {
        private Games g;
        public fm()
        {
            InitializeComponent();
            g = new Games();
            g.GoScreenRemember += Event_GoScreenRemember;
            g.GoScreenAnswer += Event_GoScreenAnswer;
            g.DoReset();
            buGo.Click += BuGo_Click;
            tbVvod.KeyDown += tbVVod_KeyDown;
        }

        private void BuGo_Click(object sender, EventArgs e)
        {
            int x;
            int.TryParse(tbVvod.Text, out x);
            g.DoAnswer(x);
        }

        private void Event_GoScreenAnswer(object sender, EventArgs e)
        {
            tbVvod.Text = "";
            laChislo.Visible = false;
            laZapomni.Visible = false;
            buGo.Visible = true;
            laVspomni.Visible = true;
            tbVvod.Visible = true;
        }

        private void Event_GoScreenRemember(object sender, EventArgs e)
        {
            laChislo.Visible = true;
            laZapomni.Visible = true;
            buGo.Visible = false;
            laVspomni.Visible = false;
            tbVvod.Visible = false;
            laYes.Text =  String.Format("Верно = {0}", g.CountCorrect.ToString());
            laNo.Text = String.Format("Неверно = {0}", g.CountWrong.ToString());
            laChislo.Text = g.Code.ToString();
            

        }
        private void tbVVod_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int x;
                int.TryParse(tbVvod.Text, out x);
                g.DoAnswer(x);
            }
        }
    }
}
