﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue x;
            x = new Queue();
            x.Enqueue("Privet");
            x.Enqueue(", kak");
            x.Enqueue(" dela?");
            
            Console.WriteLine(x.Peek());
            Console.WriteLine("------");
            while (x.Count > 0)
            {
                string v = (string)x.Dequeue();
                Console.Write(v);
            }
            Console.WriteLine();
        }
    }
}
