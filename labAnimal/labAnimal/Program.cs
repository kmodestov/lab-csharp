﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labAnimal
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat x1;
            x1 = new Cat();
            x1.Name = "Барсик";
            Console.WriteLine(x1.Name);
            Console.WriteLine(x1.Legs);
            Console.WriteLine("---------");
            Animal x;
            x = new Cat();
            Console.WriteLine(x.Legs);
            Cat xx = (Cat)x;
            Console.WriteLine(xx.Name);
        }
    }
}
