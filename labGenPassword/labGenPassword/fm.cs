﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using labGenPassowrd.Core;

namespace labGenPassword
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();
            buPassword.Click += BuPassword_Click;
        }

        private void BuPassword_Click(object sender, EventArgs e)
        {
            edPassword.Text = Utils.RandomStr((int)edLenght.Value, ckLower.Checked, ckUpper.Checked, ckNumber.Checked, ckSpec.Checked);
            tbLog.Text += edPassword.Text + tbLog.Text;
            tbLog.Text += Keys.PageDown;
        }
    }
}
