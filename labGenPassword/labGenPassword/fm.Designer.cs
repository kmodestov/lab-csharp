﻿namespace labGenPassword
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edPassword = new System.Windows.Forms.TextBox();
            this.buPassword = new System.Windows.Forms.Button();
            this.ckLower = new System.Windows.Forms.CheckBox();
            this.ckUpper = new System.Windows.Forms.CheckBox();
            this.ckNumber = new System.Windows.Forms.CheckBox();
            this.ckSpec = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.edLenght = new System.Windows.Forms.NumericUpDown();
            this.tbLog = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.edLenght)).BeginInit();
            this.SuspendLayout();
            // 
            // edPassword
            // 
            this.edPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edPassword.Location = new System.Drawing.Point(57, 61);
            this.edPassword.Multiline = true;
            this.edPassword.Name = "edPassword";
            this.edPassword.Size = new System.Drawing.Size(298, 47);
            this.edPassword.TabIndex = 0;
            this.edPassword.Text = "<Password>";
            this.edPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buPassword
            // 
            this.buPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buPassword.Location = new System.Drawing.Point(57, 150);
            this.buPassword.Name = "buPassword";
            this.buPassword.Size = new System.Drawing.Size(298, 56);
            this.buPassword.TabIndex = 1;
            this.buPassword.Text = "Генерировать";
            this.buPassword.UseVisualStyleBackColor = true;
            // 
            // ckLower
            // 
            this.ckLower.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ckLower.AutoSize = true;
            this.ckLower.Location = new System.Drawing.Point(43, 242);
            this.ckLower.Name = "ckLower";
            this.ckLower.Size = new System.Drawing.Size(174, 17);
            this.ckLower.TabIndex = 2;
            this.ckLower.Text = "Символы в нижнем регистре";
            this.ckLower.UseVisualStyleBackColor = true;
            // 
            // ckUpper
            // 
            this.ckUpper.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ckUpper.AutoSize = true;
            this.ckUpper.Location = new System.Drawing.Point(43, 265);
            this.ckUpper.Name = "ckUpper";
            this.ckUpper.Size = new System.Drawing.Size(177, 17);
            this.ckUpper.TabIndex = 3;
            this.ckUpper.Text = "Символы в верхнем регистре";
            this.ckUpper.UseVisualStyleBackColor = true;
            // 
            // ckNumber
            // 
            this.ckNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ckNumber.AutoSize = true;
            this.ckNumber.Location = new System.Drawing.Point(43, 288);
            this.ckNumber.Name = "ckNumber";
            this.ckNumber.Size = new System.Drawing.Size(62, 17);
            this.ckNumber.TabIndex = 4;
            this.ckNumber.Text = "Цифры";
            this.ckNumber.UseVisualStyleBackColor = true;
            // 
            // ckSpec
            // 
            this.ckSpec.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ckSpec.AutoSize = true;
            this.ckSpec.Location = new System.Drawing.Point(43, 311);
            this.ckSpec.Name = "ckSpec";
            this.ckSpec.Size = new System.Drawing.Size(144, 17);
            this.ckSpec.TabIndex = 5;
            this.ckSpec.Text = "Специальные символы";
            this.ckSpec.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(40, 344);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(451, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Длина пароля";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // edLenght
            // 
            this.edLenght.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edLenght.Location = new System.Drawing.Point(193, 349);
            this.edLenght.Name = "edLenght";
            this.edLenght.Size = new System.Drawing.Size(424, 20);
            this.edLenght.TabIndex = 7;
            // 
            // tbLog
            // 
            this.tbLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLog.Location = new System.Drawing.Point(481, 61);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.Size = new System.Drawing.Size(220, 145);
            this.tbLog.TabIndex = 8;
            // 
            // fm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(713, 424);
            this.Controls.Add(this.tbLog);
            this.Controls.Add(this.edLenght);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ckSpec);
            this.Controls.Add(this.ckNumber);
            this.Controls.Add(this.ckUpper);
            this.Controls.Add(this.ckLower);
            this.Controls.Add(this.buPassword);
            this.Controls.Add(this.edPassword);
            this.MinimumSize = new System.Drawing.Size(422, 463);
            this.Name = "fm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "labGenPassowrd";
            ((System.ComponentModel.ISupportInitialize)(this.edLenght)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edPassword;
        private System.Windows.Forms.Button buPassword;
        private System.Windows.Forms.CheckBox ckLower;
        private System.Windows.Forms.CheckBox ckUpper;
        private System.Windows.Forms.CheckBox ckNumber;
        private System.Windows.Forms.CheckBox ckSpec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown edLenght;
        private System.Windows.Forms.TextBox tbLog;
    }
}

