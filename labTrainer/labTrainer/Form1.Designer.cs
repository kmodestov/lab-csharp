﻿namespace labTrainer
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.laVerno = new System.Windows.Forms.Label();
            this.laNeverno = new System.Windows.Forms.Label();
            this.laVyvod = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buDa = new System.Windows.Forms.Button();
            this.buNet = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.laNeverno, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.laVerno, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(409, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // laVerno
            // 
            this.laVerno.AutoSize = true;
            this.laVerno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.laVerno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laVerno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laVerno.Location = new System.Drawing.Point(3, 0);
            this.laVerno.Name = "laVerno";
            this.laVerno.Size = new System.Drawing.Size(198, 100);
            this.laVerno.TabIndex = 0;
            this.laVerno.Text = "Верно=0";
            this.laVerno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laNeverno
            // 
            this.laNeverno.AutoSize = true;
            this.laNeverno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.laNeverno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laNeverno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laNeverno.Location = new System.Drawing.Point(207, 0);
            this.laNeverno.Name = "laNeverno";
            this.laNeverno.Size = new System.Drawing.Size(199, 100);
            this.laNeverno.TabIndex = 1;
            this.laNeverno.Text = "Неверно=0";
            this.laNeverno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laVyvod
            // 
            this.laVyvod.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laVyvod.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laVyvod.Location = new System.Drawing.Point(12, 128);
            this.laVyvod.Name = "laVyvod";
            this.laVyvod.Size = new System.Drawing.Size(409, 120);
            this.laVyvod.TabIndex = 1;
            this.laVyvod.Text = "10 + 11 = 21";
            this.laVyvod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 237);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(409, 59);
            this.label2.TabIndex = 2;
            this.label2.Text = "Верно?";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.buNet, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.buDa, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 299);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(409, 61);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // buDa
            // 
            this.buDa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buDa.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buDa.ForeColor = System.Drawing.Color.Green;
            this.buDa.Location = new System.Drawing.Point(3, 3);
            this.buDa.Name = "buDa";
            this.buDa.Size = new System.Drawing.Size(198, 55);
            this.buDa.TabIndex = 0;
            this.buDa.Text = "Да";
            this.buDa.UseVisualStyleBackColor = true;
            this.buDa.Click += new System.EventHandler(this.buDa_Click);
            // 
            // buNet
            // 
            this.buNet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buNet.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buNet.ForeColor = System.Drawing.Color.Red;
            this.buNet.Location = new System.Drawing.Point(207, 3);
            this.buNet.Name = "buNet";
            this.buNet.Size = new System.Drawing.Size(199, 55);
            this.buNet.TabIndex = 1;
            this.buNet.Text = "Нет";
            this.buNet.UseVisualStyleBackColor = true;
            this.buNet.Click += new System.EventHandler(this.buNet_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 372);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.laVyvod);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(449, 410);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label laNeverno;
        private System.Windows.Forms.Label laVerno;
        private System.Windows.Forms.Label laVyvod;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button buNet;
        private System.Windows.Forms.Button buDa;
    }
}

