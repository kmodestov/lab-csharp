﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTrainer
{
    public partial class Form1 : Form
    {
        private Game g;
        public Form1()
        {
            InitializeComponent();
            g = new Game();
            g.Change += Event_Change;
            g.DoReset();
        }
        private void Event_Change(object sender, EventArgs e)
        {
            laVerno.Text = String.Format("Верно = {0}", g.CountCorrect.ToString());
            laNeverno.Text = String.Format("Неверно = {0}", g.CountWrong.ToString());
            laVyvod.Text = g.CodeText;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buDa_Click(object sender, EventArgs e)
        {
            g.DoAnswer(true);
        }

        private void buNet_Click(object sender, EventArgs e)
        {
            g.DoAnswer(false);
        }
    }
}
