﻿namespace labPoezia
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm));
            this.tc = new System.Windows.Forms.TabControl();
            this.tc1 = new System.Windows.Forms.TabPage();
            this.meText = new System.Windows.Forms.TextBox();
            this.tc2 = new System.Windows.Forms.TabPage();
            this.meThoughLine = new System.Windows.Forms.TextBox();
            this.tc3 = new System.Windows.Forms.TabPage();
            this.meFirstWords = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.ts = new System.Windows.Forms.ToolStrip();
            this.buZoomOut = new System.Windows.Forms.ToolStripButton();
            this.buZoomIn = new System.Windows.Forms.ToolStripButton();
            this.buInfo = new System.Windows.Forms.ToolStripButton();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.meLastWords = new System.Windows.Forms.TextBox();
            this.tc.SuspendLayout();
            this.tc1.SuspendLayout();
            this.tc2.SuspendLayout();
            this.tc3.SuspendLayout();
            this.ts.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tc
            // 
            this.tc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tc.Controls.Add(this.tc1);
            this.tc.Controls.Add(this.tc2);
            this.tc.Controls.Add(this.tc3);
            this.tc.Controls.Add(this.tabPage1);
            this.tc.Location = new System.Drawing.Point(1, 28);
            this.tc.Name = "tc";
            this.tc.SelectedIndex = 0;
            this.tc.Size = new System.Drawing.Size(442, 445);
            this.tc.TabIndex = 0;
            // 
            // tc1
            // 
            this.tc1.Controls.Add(this.meText);
            this.tc1.Location = new System.Drawing.Point(4, 22);
            this.tc1.Name = "tc1";
            this.tc1.Padding = new System.Windows.Forms.Padding(3);
            this.tc1.Size = new System.Drawing.Size(434, 419);
            this.tc1.TabIndex = 0;
            this.tc1.Text = "Полностью";
            this.tc1.UseVisualStyleBackColor = true;
            // 
            // meText
            // 
            this.meText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meText.Location = new System.Drawing.Point(3, 3);
            this.meText.Multiline = true;
            this.meText.Name = "meText";
            this.meText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.meText.Size = new System.Drawing.Size(428, 413);
            this.meText.TabIndex = 1;
            this.meText.Text = resources.GetString("meText.Text");
            // 
            // tc2
            // 
            this.tc2.Controls.Add(this.meThoughLine);
            this.tc2.Location = new System.Drawing.Point(4, 22);
            this.tc2.Name = "tc2";
            this.tc2.Padding = new System.Windows.Forms.Padding(3);
            this.tc2.Size = new System.Drawing.Size(434, 419);
            this.tc2.TabIndex = 1;
            this.tc2.Text = "Через строку";
            this.tc2.UseVisualStyleBackColor = true;
            // 
            // meThoughLine
            // 
            this.meThoughLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meThoughLine.Location = new System.Drawing.Point(3, 3);
            this.meThoughLine.Multiline = true;
            this.meThoughLine.Name = "meThoughLine";
            this.meThoughLine.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.meThoughLine.Size = new System.Drawing.Size(428, 413);
            this.meThoughLine.TabIndex = 1;
            // 
            // tc3
            // 
            this.tc3.Controls.Add(this.meFirstWords);
            this.tc3.Controls.Add(this.textBox1);
            this.tc3.Location = new System.Drawing.Point(4, 22);
            this.tc3.Name = "tc3";
            this.tc3.Padding = new System.Windows.Forms.Padding(3);
            this.tc3.Size = new System.Drawing.Size(434, 419);
            this.tc3.TabIndex = 2;
            this.tc3.Text = "Первые слова";
            this.tc3.UseVisualStyleBackColor = true;
            // 
            // meFirstWords
            // 
            this.meFirstWords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meFirstWords.Location = new System.Drawing.Point(3, 3);
            this.meFirstWords.Multiline = true;
            this.meFirstWords.Name = "meFirstWords";
            this.meFirstWords.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.meFirstWords.Size = new System.Drawing.Size(428, 413);
            this.meFirstWords.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(428, 413);
            this.textBox1.TabIndex = 0;
            // 
            // ts
            // 
            this.ts.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buZoomOut,
            this.buZoomIn,
            this.buInfo});
            this.ts.Location = new System.Drawing.Point(0, 0);
            this.ts.Name = "ts";
            this.ts.Size = new System.Drawing.Size(442, 25);
            this.ts.TabIndex = 1;
            this.ts.Text = "toolStrip1";
            // 
            // buZoomOut
            // 
            this.buZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("buZoomOut.Image")));
            this.buZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buZoomOut.Name = "buZoomOut";
            this.buZoomOut.Size = new System.Drawing.Size(66, 22);
            this.buZoomOut.Text = "Zoom Out";
            // 
            // buZoomIn
            // 
            this.buZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("buZoomIn.Image")));
            this.buZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buZoomIn.Name = "buZoomIn";
            this.buZoomIn.Size = new System.Drawing.Size(56, 22);
            this.buZoomIn.Text = "Zoom In";
            // 
            // buInfo
            // 
            this.buInfo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buInfo.Image = ((System.Drawing.Image)(resources.GetObject("buInfo.Image")));
            this.buInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buInfo.Name = "buInfo";
            this.buInfo.Size = new System.Drawing.Size(86, 22);
            this.buInfo.Text = "О программе";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.meLastWords);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(434, 419);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Последние слова";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // meLastWords
            // 
            this.meLastWords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meLastWords.Location = new System.Drawing.Point(3, 3);
            this.meLastWords.Multiline = true;
            this.meLastWords.Name = "meLastWords";
            this.meLastWords.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.meLastWords.Size = new System.Drawing.Size(428, 413);
            this.meLastWords.TabIndex = 2;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 474);
            this.Controls.Add(this.ts);
            this.Controls.Add(this.tc);
            this.Name = "fm";
            this.Text = "labPoeziya";
            this.tc.ResumeLayout(false);
            this.tc1.ResumeLayout(false);
            this.tc1.PerformLayout();
            this.tc2.ResumeLayout(false);
            this.tc2.PerformLayout();
            this.tc3.ResumeLayout(false);
            this.tc3.PerformLayout();
            this.ts.ResumeLayout(false);
            this.ts.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tc;
        private System.Windows.Forms.TabPage tc1;
        private System.Windows.Forms.TextBox meText;
        private System.Windows.Forms.TabPage tc2;
        private System.Windows.Forms.TextBox meThoughLine;
        private System.Windows.Forms.TabPage tc3;
        private System.Windows.Forms.TextBox meFirstWords;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStrip ts;
        private System.Windows.Forms.ToolStripButton buZoomOut;
        private System.Windows.Forms.ToolStripButton buZoomIn;
        private System.Windows.Forms.ToolStripButton buInfo;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox meLastWords;
    }
}

