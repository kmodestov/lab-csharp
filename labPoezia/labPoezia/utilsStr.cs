﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labPoezia
{
    static class utilsStr
    {
        public static string[] ThroughLine(string[] v)
        {
            var x = new StringBuilder();
            string[] xResult = new string[v.Length];
            for (int i = 0; i < v.Length; i++)
            {
                x.Clear();
                x.Append(v[i]);
                if (i % 2 == 1)
                {
                    for (int j = 0; j < x.Length; j++)
                    {
                        if (x[j] != ' ')
                            x[j] = 'x';
                    }
                }
                xResult[i] = x.ToString();
            }
            return xResult;
        }

        public static string[] FirstWords(string[] v)
        {
            var x = new StringBuilder();
            string[] xResult = new string[v.Length];
            bool xFlag;
            for (int i = 0; i < v.Length; i++)
            {
                x.Clear();
                x.Append(v[i]);
                xFlag = false;                            
                for (int j = 0; j < x.Length; j++)
                    {
                    if ((xFlag) && (x[j] != ' '))
                        x[j] = 'x';
                    if ((!xFlag) && (x[j] == ' '))
                        xFlag = true;
                    }
                
                xResult[i] = x.ToString();
            }
            return xResult;
        }
        public static string[] LastWords(string[] v)
        {
            var x = new StringBuilder();
            string[] xResult = new string[v.Length];
            bool xFlag;
            for (int i = 0; i < v.Length; i++)
            {
                x.Clear();
                x.Append(v[i]);
                xFlag = false;
                for (int j = x.Length; j < 0; j--)
                {
                    if ((xFlag) && (x[j] != ' '))
                        x[j] = 'x';
                    if ((!xFlag) && (x[j] != ' '))
                        xFlag = true;

                }
                xResult[i] = x.ToString();
            }
            return xResult;
        }
    }
}
