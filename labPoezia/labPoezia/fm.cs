﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPoezia
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();
            tc.TabIndex = 0;
            meThoughLine.Lines = utilsStr.ThroughLine(meText.Lines);
            meFirstWords.Lines = utilsStr.FirstWords(meText.Lines);
            meLastWords.Lines = utilsStr.LastWords(meText.Lines);
            buZoomOut.Click += BuZoomOut_Click; ;
            buZoomIn.Click += BuZoomIn_Click;
            buInfo.Click += BuInfo_Click;
        }

        private void BuInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("labPoeziya - Toroptsev A.N.");
        }

        private void BuZoomOut_Click(object sender, EventArgs e)
        {
            float x = meText.Font.Size;
            x -= 1;
            meText.Font = new Font(meText.Font.FontFamily, x);
            meThoughLine.Font = new Font(meThoughLine.Font.FontFamily, x);
            meFirstWords.Font = new Font(meFirstWords.Font.FontFamily, x);
        }

        private void BuZoomIn_Click(object sender, EventArgs e)
        {
            float x = meText.Font.Size;
            x += 1;
            meText.Font = new Font(meText.Font.FontFamily, x);
            meThoughLine.Font = new Font(meThoughLine.Font.FontFamily, x);
            meFirstWords.Font = new Font(meFirstWords.Font.FontFamily, x);
        }
    }
}
