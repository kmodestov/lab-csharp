﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labSwitch
{
    class Program
    {
        static void Main(string[] args)
        {
            AAA(3, 2, '+');
            AAA(3, 2, '-');

        }

        private static void AAA(int v1, int v2, char v3)
        {
            int r = 0;
            switch (v3)
            {
                case '+':
                    r = v1 + v2;
                    break;
                case '-':
                    r = v1 - v2;
                    break;
                default:
                    Console.WriteLine("Неизвестный оператор");
                    return;
            }
            Console.WriteLine("{0} {1} {2} = {3}", v1, v3, v2, r);
        }
    }
}
