﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labStruct
{
    class Program
    {
        static void Main(string[] args)
        {

            Queue y;
            y = new Queue();
            Point x;
            x.X = "Москва";
            x.Y = "Россия";
            y.Enqueue(x);
            x.X = "Лондон";
            x.Y = "Англия";
            y.Enqueue(x);
            while (y.Count > 0)
            {
                Point v = (Point)y.Dequeue();
                Console.WriteLine(v.X + " " + v.Y);
            }
            
            
        }
    }

    struct Point
    {
        public string X, Y;
    }
}
