﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labString
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "Саша";
            int age = 22;
            string s1 = $"Привет, {name}! Тебе уже {age} года";
            string s2 = $"В Москве - {DateTime.Now:hh:mm}";
            Console.WriteLine(s1);
            Console.WriteLine(s2);

        }
    }
}
