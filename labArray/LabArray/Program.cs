﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] x = { 1, 2, 3, 4 };
            Console.WriteLine(x[1]);
            Console.WriteLine(x[3] + x[2]);
            String[] y = { "1", "2", "3", "4" };
            Console.WriteLine(y[1]);
            
        }
    }
}
