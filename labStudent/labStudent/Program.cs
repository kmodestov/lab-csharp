﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labStudent
{
    class Program
    {
        static void Main(string[] args)
        {
            Student x = new Student();
            x.ChangeAge += myEvent;
            x.Privet += myEvent2;
            x.Name = "Иван";
            x.Surname = "Иванович";
            x.Age = 16;
            x.Nomer = 1;
            Console.WriteLine(x.GetFullName());
            x.Age = 18;
            x.Nomer = 2;
            Console.WriteLine(x.GetFullName());
            x.Age = 17;
            x.Nomer = 3;
            Console.WriteLine(x.GetFullName());
            Console.WriteLine("----------------");
            Student student = new Student() { Name = "Иван", Surname = "Иванович", Age = 17 };
            Console.WriteLine(student.GetFullName());
        }
        static public void myEvent(object sender, EventArgs e)
        {
            Console.WriteLine("Изменился возраст");
        }
        static public void myEvent2(object sender, EventArgs y)
        {
            Console.WriteLine("Ты третий!");
        }
    }

}
