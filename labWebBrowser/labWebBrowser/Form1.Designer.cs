﻿namespace labWebBrowser
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edUrl = new System.Windows.Forms.TextBox();
            this.buGo = new System.Windows.Forms.Button();
            this.buForward = new System.Windows.Forms.Button();
            this.buRefresh = new System.Windows.Forms.Button();
            this.buStop = new System.Windows.Forms.Button();
            this.buBack = new System.Windows.Forms.Button();
            this.wb = new System.Windows.Forms.WebBrowser();
            this.buInfo = new System.Windows.Forms.Button();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // edUrl
            // 
            this.edUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edUrl.Location = new System.Drawing.Point(12, 12);
            this.edUrl.Name = "edUrl";
            this.edUrl.Size = new System.Drawing.Size(432, 20);
            this.edUrl.TabIndex = 0;
            this.edUrl.Text = "https://ya.ru/";
            // 
            // buGo
            // 
            this.buGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buGo.Location = new System.Drawing.Point(450, 12);
            this.buGo.Name = "buGo";
            this.buGo.Size = new System.Drawing.Size(75, 23);
            this.buGo.TabIndex = 1;
            this.buGo.Text = "Go!";
            this.buGo.UseVisualStyleBackColor = true;
            // 
            // buForward
            // 
            this.buForward.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buForward.Location = new System.Drawing.Point(151, 486);
            this.buForward.Name = "buForward";
            this.buForward.Size = new System.Drawing.Size(126, 23);
            this.buForward.TabIndex = 2;
            this.buForward.Text = ">>";
            this.buForward.UseVisualStyleBackColor = true;
            // 
            // buRefresh
            // 
            this.buRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buRefresh.Location = new System.Drawing.Point(283, 486);
            this.buRefresh.Name = "buRefresh";
            this.buRefresh.Size = new System.Drawing.Size(128, 23);
            this.buRefresh.TabIndex = 3;
            this.buRefresh.Text = "Refresh";
            this.buRefresh.UseVisualStyleBackColor = true;
            // 
            // buStop
            // 
            this.buStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buStop.Location = new System.Drawing.Point(417, 486);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(108, 23);
            this.buStop.TabIndex = 4;
            this.buStop.Text = "Stop";
            this.buStop.UseVisualStyleBackColor = true;
            // 
            // buBack
            // 
            this.buBack.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buBack.Location = new System.Drawing.Point(12, 486);
            this.buBack.Name = "buBack";
            this.buBack.Size = new System.Drawing.Size(133, 23);
            this.buBack.TabIndex = 5;
            this.buBack.Text = "<<";
            this.buBack.UseVisualStyleBackColor = true;
            // 
            // wb
            // 
            this.wb.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wb.Location = new System.Drawing.Point(12, 50);
            this.wb.MinimumSize = new System.Drawing.Size(20, 20);
            this.wb.Name = "wb";
            this.wb.Size = new System.Drawing.Size(525, 434);
            this.wb.TabIndex = 6;
            // 
            // buInfo
            // 
            this.buInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buInfo.Location = new System.Drawing.Point(12, 515);
            this.buInfo.Name = "buInfo";
            this.buInfo.Size = new System.Drawing.Size(127, 23);
            this.buInfo.TabIndex = 7;
            this.buInfo.Text = "Об авторе";
            this.buInfo.UseVisualStyleBackColor = true;
            // 
            // tbInfo
            // 
            this.tbInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbInfo.Location = new System.Drawing.Point(145, 517);
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.Size = new System.Drawing.Size(380, 20);
            this.tbInfo.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 542);
            this.Controls.Add(this.tbInfo);
            this.Controls.Add(this.buInfo);
            this.Controls.Add(this.wb);
            this.Controls.Add(this.buBack);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buRefresh);
            this.Controls.Add(this.buForward);
            this.Controls.Add(this.buGo);
            this.Controls.Add(this.edUrl);
            this.MinimumSize = new System.Drawing.Size(542, 539);
            this.Name = "Form1";
            this.Text = "webBrowser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edUrl;
        private System.Windows.Forms.Button buGo;
        private System.Windows.Forms.Button buForward;
        private System.Windows.Forms.Button buRefresh;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Button buBack;
        private System.Windows.Forms.WebBrowser wb;
        private System.Windows.Forms.Button buInfo;
        private System.Windows.Forms.TextBox tbInfo;
    }
}

