﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labWebBrowser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            buGo.Click += delegate
            {
                wb.Navigate(edUrl.Text);
            };
            buBack.Click += delegate
            {
                wb.GoBack();
            };
            buForward.Click += delegate
            {
                wb.GoForward();
            };
            buRefresh.Click += delegate
            {
                wb.Refresh();
            };
            buStop.Click += delegate
            {
                wb.Stop();
            };
            wb.DocumentCompleted += delegate
            {
                edUrl.Text = wb.Url.ToString();
            };
            edUrl.KeyDown += EdUrl_KeyDown;
            buInfo.Click += delegate
            {
                tbInfo.Text = ("Торопцев А.Н. 2017");
            };

        }
        private void EdUrl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                wb.Navigate(edUrl.Text);
        }

    }
}
